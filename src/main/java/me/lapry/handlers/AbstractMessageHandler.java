package me.lapry.handlers;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * 
 * Syntax for extending this class is "public class YourMessageHandler extends AbstractMessageHandler"
 * 
 * I prefer to have an EntityPlayer readily available when handling packets, as well as to
 * know which side I'm on without having to check every time, so I handle those operations
 * here and pass off the rest of the work to abstract methods to be handled in each sub-class.
 * 
 * We do not want to have to implement client handling for server side messages (and vice-versa),
 * however, so we will abstractify even further, as well as create separate packages to organize
 * our client vs. server messages. If you only have a few packets, you may opt not to, but once
 * you have more than a handful, keeping them separate makes it easier to remember on which side
 * to register, which side you can send to, and so on.
 *
 */
public abstract class AbstractMessageHandler<T extends IMessage> implements IMessageHandler <T, IMessage>{
	
	 /**
	 * 
	 * This is just a convenience class that will prevent the server-side message handling
	 * method from appearing in our client message handler classes.
	 *
	 **/
	public static abstract class AbstractClientMessageHandler<T extends IMessage> extends AbstractMessageHandler<T>{
		/**
		 * Handle a message received on the client side
		 * @return a message to send back to the Server, or null if no reply is necessary
		 */
		 @SideOnly(Side.CLIENT)
		 public abstract IMessage handleClientMessage(EntityPlayer player, T message, MessageContext ctx);
		
		// implementing a final version of the server message handler both prevents it from
		// appearing automatically and prevents us from ever accidentally overriding it
		 
	}
	
	
	 /**
	 * 
	 * This is just a convenience class that will prevent the client-side message handling
	 * method from appearing in our server message handler classes.
	 *
	 */
	// class for messages sent to and handled on the server:
	public static abstract class AbstractServerMessageHandler<T extends IMessage> extends AbstractMessageHandler<T>{
		

		/**
		 * Handle a message received on the server side
		 * @return a message to send back to the Client, or null if no reply is necessary
		 */
		 public abstract IMessage handleServerMessage(EntityPlayer player, T message, MessageContext ctx);

		
	}
	
	 /**
	 * For messages which can be sent to either Side
	 */
	public static abstract class AbstractBiMessageHandler<T extends IMessage> extends AbstractMessageHandler<T> {

	}
}