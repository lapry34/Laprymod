package me.lapry.commands;

import java.util.Collections;
import java.util.List;

import joptsimple.internal.Strings;
import me.lapry.utils.CommandHelper;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;

public class HealCommand extends CommandBase {

	@Override
	public String getCommandName() {
		return "heal";
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return Strings.EMPTY;
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) throws CommandException {

		CommandHelper helper = new CommandHelper(sender);
		
		EntityPlayer player = helper.getPlayer();

		player.setHealth(20);
	
	}
	
	@Override
	public List<String> getCommandAliases(){
		return Collections.emptyList();
	}
	
}
