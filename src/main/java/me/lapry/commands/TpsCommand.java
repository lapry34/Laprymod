package me.lapry.commands;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;

import joptsimple.internal.Strings;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraftforge.common.DimensionManager;

public class TpsCommand extends CommandBase {

	private static final DecimalFormat timeFormatter = new DecimalFormat("########0.000");
	private WeakReference<MinecraftServer> server = new WeakReference<MinecraftServer>(MinecraftServer.getServer());
	
	@Override
	public String getCommandName(){
		return "tps";
	}

	@Override
	public String getCommandUsage(ICommandSender sender){
		return Strings.EMPTY;
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
		
		 int dim = 0;
	        boolean summary = true;
	        if (args.length > 1){
	            dim = parseInt(args[1]);
	            summary = false;
	        }
	        if (summary){
	            for (Integer dimId : DimensionManager.getIDs()){
	                double worldTickTime = mean(this.getServer().worldTickTimes.get(dimId)) * 1.0E-6D;
	                double worldTPS = Math.min(1000.0/worldTickTime, 20);
	                sender.addChatMessage(new ChatComponentTranslation("commands.forge.tps.summary",String.format("Dim %d", dimId), timeFormatter.format(worldTickTime), timeFormatter.format(worldTPS)));
	            }
	            double meanTickTime = mean(this.getServer().tickTimeArray) * 1.0E-6D;
	            double meanTPS = Math.min(1000.0/meanTickTime, 20);
	            sender.addChatMessage(new ChatComponentTranslation("commands.forge.tps.summary","Overall", timeFormatter.format(meanTickTime), timeFormatter.format(meanTPS)));
	        }
	        else {
	            double worldTickTime = mean(this.getServer().worldTickTimes.get(dim)) * 1.0E-6D;
	            double worldTPS = Math.min(1000.0/worldTickTime, 20);
	            sender.addChatMessage(new ChatComponentTranslation("commands.forge.tps.summary",String.format("Dim %d", dim), timeFormatter.format(worldTickTime), timeFormatter.format(worldTPS)));
	        }
	}
	
	private static long mean(long[] values){
        long sum = 0l;
        for (long v : values)
            sum+=v;

        return sum / values.length;
    }
	
	private MinecraftServer getServer(){
        return this.server.get();
    }
	

	@Override
	public List<String> getCommandAliases(){
		return Collections.emptyList();
	}

}
