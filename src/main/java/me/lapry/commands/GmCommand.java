package me.lapry.commands;

import java.util.Collections;
import java.util.List;

import joptsimple.internal.Strings;
import me.lapry.utils.CommandHelper;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.WorldSettings.GameType;

public class GmCommand extends CommandBase {

	@Override
	public String getCommandName() {
		return "gm";
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return Strings.EMPTY;
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
		
		CommandHelper helper = new CommandHelper(sender);
		
		EntityPlayer player = helper.getPlayer();
		
		if(args.length > 0){
			
			if(args[0] == "0")
				player.setGameType(GameType.SURVIVAL);
			else if(args[0] == "1")
				player.setGameType(GameType.CREATIVE);
			else if(args[0] == "2")
				player.setGameType(GameType.ADVENTURE);
			else if(args[0] == "3")
				player.setGameType(GameType.SPECTATOR);
			}
		else
			helper.sendMessage("&6usage /gm <number>");
	}

	@Override
	public List<String> getCommandAliases(){
		return Collections.emptyList();
	}
}
