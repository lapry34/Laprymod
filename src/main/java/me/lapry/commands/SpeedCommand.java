package me.lapry.commands;

import java.util.Collections;
import java.util.List;

import joptsimple.internal.Strings;
import me.lapry.utils.CommandHelper;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import scala.Int;

public class SpeedCommand extends CommandBase {

	@Override
	public String getCommandName() {
		return "speed";
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return Strings.EMPTY;
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
		
		CommandHelper helper = new CommandHelper(sender);
		
		EntityPlayer player = helper.getPlayer();
		
		if(args.length < 0)
			player.clearActivePotions();
		else if(args[0] == "1")
			player.addPotionEffect(new PotionEffect(Potion.moveSpeed.getId(), Int.MaxValue(), 1, true, false));
		else if(args[0] == "2")
			player.addPotionEffect(new PotionEffect(Potion.moveSpeed.getId(), Int.MaxValue(), 2, true, false));
		else if(args[0] == "3")
			player.addPotionEffect(new PotionEffect(Potion.moveSpeed.getId(), Int.MaxValue(), 3, true, false));
		else if(args[0] == "4")
			player.addPotionEffect(new PotionEffect(Potion.moveSpeed.getId(), Int.MaxValue(), 4, true, false));
		else if(args[0] == "5")
			player.addPotionEffect(new PotionEffect(Potion.moveSpeed.getId(), Int.MaxValue(), 5, true, false));
	
	}

	@Override
	public List<String> getCommandAliases(){
		return Collections.emptyList();
	}
	
}
