package me.lapry.commands;

import java.util.List;

import com.google.common.collect.Lists;

import joptsimple.internal.Strings;
import me.lapry.utils.CommandHelper;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;


public class VanishCommand extends CommandBase {

	@Override
	public String getCommandName() {
		return "vanish";
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return Strings.EMPTY;
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) throws CommandException {

		CommandHelper helper = new CommandHelper(sender);
		
		EntityPlayer player = helper.getPlayer();
	
		if(!player.isInvisible())
			player.setInvisible(true);
		else 
			player.setInvisible(false);
	
	}

	@Override
	public List<String> getCommandAliases(){
		
		List<String> list = Lists.newArrayList();
		
		list.add("v");
		
		return list;
	}
	
}
