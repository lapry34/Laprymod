package me.lapry.commands;

import java.util.Collections;
import java.util.List;

import joptsimple.internal.Strings;
import me.lapry.utils.CommandHelper;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentTranslation;

public class SuicideCommand extends CommandBase {

	@Override
	public String getCommandName() {
		return "suicide";
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return Strings.EMPTY;
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
	
		CommandHelper helper = new CommandHelper(sender);
		
		EntityPlayer player = helper.getPlayer();
		
		player.onKillCommand();
		
		MinecraftServer.getServer().addChatMessage(new ChatComponentTranslation(String.format(player.getDisplayNameString() + "&3 Si � Suicidato").replaceAll("&", "�")));
	}
	
	@Override
	public List<String> getCommandAliases(){
		return Collections.emptyList();
	}
	
}
