package me.lapry;

import java.io.File;
import java.net.URL;
import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.eventbus.EventBus;

import joptsimple.internal.Strings;
import me.lapry.utils.References;
import net.minecraftforge.fml.common.DummyModContainer;
import net.minecraftforge.fml.common.LoadController;
import net.minecraftforge.fml.common.MetadataCollection;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.ModMetadata;
import net.minecraftforge.fml.common.versioning.ArtifactVersion;
import net.minecraftforge.fml.common.versioning.InvalidVersionSpecificationException;
import net.minecraftforge.fml.common.versioning.VersionRange;

public class LapryModContainer extends DummyModContainer implements ModContainer {
	
	@Override
	public String getModId(){
		return References.MODID;
	}

	@Override
	public String getName(){
		return References.MOD_NAME;
	}

	@Override
	public String getVersion(){
		return References.VERSION;
	}

	@Deprecated
	@Override
	public File getSource(){
		return null;
	}

	@Deprecated
	@Override
	public ModMetadata getMetadata(){
		return null;
	}

	@Override
	public void bindMetadata(MetadataCollection mc){}

	@Override
	public void setEnabledState(boolean enabled){}

	@Override
	public Set<ArtifactVersion> getRequirements(){
		return Collections.emptySet();
	}

	@Override
	public List<ArtifactVersion> getDependencies(){
		return Collections.emptyList();
	}

	@Override
	public List<ArtifactVersion> getDependants() {
		return Collections.emptyList();
	}

	@Override
	public String getSortingRules(){
		return Strings.EMPTY;
	}

	@Override
	public boolean registerBus(EventBus bus, LoadController controller) {
		return false;
	}

	@Override
	public boolean matches(Object mod){
		return false;
	}

	@Override
	public Object getMod(){
		return Main.getInstance();
	}

	@Deprecated
	@Override
	public ArtifactVersion getProcessedVersion(){
		return null;
	}

	@Override
	public boolean isImmutable(){
		return false;
	}

	@Override
	public String getDisplayVersion(){
		return References.VERSION;
	}

	@Override
	public VersionRange acceptableMinecraftVersionRange(){
		try {
			return VersionRange.createFromVersionSpec(References.MC_VERSION);
		} catch (InvalidVersionSpecificationException e){
			return null;
		}
	}

	@Deprecated
	@Override
	public Certificate getSigningCertificate() {
		return null;
	}

	@Deprecated
	@Override
	public Map<String, String> getCustomModProperties() {
		// TODO Auto-generated method stub
		return null;
	}

	@Deprecated
	@Override
	public Class<?> getCustomResourcePackClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Deprecated
	@Override
	public Map<String, String> getSharedModDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Disableable canBeDisabled(){
		return Disableable.NEVER;
	}

	@Override
	public String getGuiClassName(){
		return Strings.EMPTY;
	}

	@Override
	public List<String> getOwnedPackages(){
		return Collections.emptyList();
	}

	@Override
	public boolean shouldLoadInEnvironment(){
		return false;
	}

	@Deprecated
	@Override
	public URL getUpdateUrl(){
		return null;
	}

}
