package me.lapry.proxy;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * 	Proxy references for a sided operations
 *  {@link ClientProxy}
 *  {@link CommonProxy}
 *  {@link ServerProxy}
**/

public abstract class CommonProxy implements IProxy {
	
	public void registerRenderThings(){};
	
	public void registerSided(){};
	
	public EntityPlayer getPlayerEntity(MessageContext ctx){
		return ctx.getServerHandler().playerEntity;
	}
}
