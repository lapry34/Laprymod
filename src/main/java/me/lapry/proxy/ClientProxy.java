package me.lapry.proxy;

import me.lapry.Main;
import me.lapry.blocks.LapryBlock;
import me.lapry.blocks.LapryOre;
import me.lapry.events.ClientTickEvent;
import me.lapry.events.KeyBindings;
import me.lapry.items.LapryIngot;
import me.lapry.items.armors.LapryBody;
import me.lapry.items.armors.LapryBoots;
import me.lapry.items.armors.LapryHelm;
import me.lapry.items.armors.LapryLegs;
import me.lapry.items.foods.GrayApple;
import me.lapry.items.tools.LapryAxe;
import me.lapry.items.tools.LapryPick;
import me.lapry.items.tools.LapryShowel;
import me.lapry.items.tools.LaprySword;
import me.lapry.utils.References;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ClientProxy extends CommonProxy {

	@Override
	public void registerRenderThings(){
	    RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();	
	    renderItem.getItemModelMesher().register(Main.lapryIngot, 0, new ModelResourceLocation(References.MODID + ":" + ((LapryIngot)Main.lapryIngot).getName(), "inventory"));
	    renderItem.getItemModelMesher().register(Main.grayApple, 0, new ModelResourceLocation(References.MODID + ":" + ((GrayApple)Main.grayApple).getName(), "inventory"));
	    renderItem.getItemModelMesher().register(Main.laprySword, 0, new ModelResourceLocation(References.MODID + ":" + ((LaprySword)Main.laprySword).getName(), "inventory"));
	    renderItem.getItemModelMesher().register(Main.lapryPick, 0, new ModelResourceLocation(References.MODID + ":" + ((LapryPick)Main.lapryPick).getName(), "inventory"));
	    renderItem.getItemModelMesher().register(Main.lapryAxe, 0, new ModelResourceLocation(References.MODID + ":" + ((LapryAxe)Main.lapryAxe).getName(), "inventory"));
	    renderItem.getItemModelMesher().register(Main.lapryShowel, 0, new ModelResourceLocation(References.MODID + ":" + ((LapryShowel)Main.lapryShowel).getName(), "inventory"));
	    renderItem.getItemModelMesher().register(Main.lapryHelm, 0, new ModelResourceLocation(References.MODID + ":" + ((LapryHelm)Main.lapryHelm).getName(), "inventory"));
	    renderItem.getItemModelMesher().register(Main.lapryBody, 0, new ModelResourceLocation(References.MODID + ":" + ((LapryBody)Main.lapryBody).getName(), "inventory"));
	    renderItem.getItemModelMesher().register(Main.lapryLegs, 0, new ModelResourceLocation(References.MODID + ":" + ((LapryLegs)Main.lapryLegs).getName(), "inventory"));
	    renderItem.getItemModelMesher().register(Main.lapryBoots, 0, new ModelResourceLocation(References.MODID + ":" +((LapryBoots)Main.lapryBoots).getName(), "inventory"));
	    renderItem.getItemModelMesher().register(Item.getItemFromBlock(Main.lapryOre), 0, new ModelResourceLocation(References.MODID + ":" + ((LapryOre)Main.lapryOre).getName(), "inventory"));
	    renderItem.getItemModelMesher().register(Item.getItemFromBlock(Main.lapryBlock), 0, new ModelResourceLocation(References.MODID + ":" + ((LapryBlock)Main.lapryBlock).getName(), "inventory"));
	    
	}
	
	@Override
	public void registerSided(){
		
		FMLCommonHandler.instance().bus().register(new ClientTickEvent());
		FMLCommonHandler.instance().bus().register(new KeyBindings());
		
	}
	
	@Override
	public EntityPlayer getPlayerEntity(MessageContext ctx) {
	 // Note that if you simply return 'Minecraft.getMinecraft().thePlayer',
	 // your packets will not work because you will be getting a client
	 // player even when you are on the server! Sounds absurd, but it's true.

	 // Solution is to double-check side before returning the player:
	 return (EntityPlayer)(ctx.side.isClient() ? Minecraft.getMinecraft().thePlayer : super.getPlayerEntity(ctx));
	}
	
}
