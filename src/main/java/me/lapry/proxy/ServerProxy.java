package me.lapry.proxy;

import me.lapry.events.ServerTickEventHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ServerProxy extends CommonProxy {

	@Override
	public void registerSided(){
		FMLCommonHandler.instance().bus().register(new ServerTickEventHandler());
	}
	
	@Override
	public EntityPlayer getPlayerEntity(MessageContext ctx) {
		 return ctx.getServerHandler().playerEntity;
	}

}
