package me.lapry.events;

import me.lapry.guis.CustomIngameMenu;
import me.lapry.guis.CustomMainMenu;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class GuiEvent {
	
	@SubscribeEvent
	public void onGuiOpen(GuiOpenEvent event){
		
		GuiScreen gui = event.gui;
		if(gui instanceof GuiMainMenu)
			event.gui = new CustomMainMenu();
		else if(gui instanceof GuiIngameMenu)
			event.gui = new CustomIngameMenu();

	}
}
