package me.lapry.events;

import me.lapry.utils.References;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class MobAttackEvent {
	
	@SubscribeEvent
	public void onMobAttacked(LivingAttackEvent event){
		
		World world = event.entityLiving.worldObj;
		
		for(int i = 0; i < 13; i++){
		
			double xPos = event.entityLiving.posX + (double)(1.0F/References.rand.nextInt(10));
			double yPos = event.entityLiving.posY + References.rand.nextInt(2);
			double zPos = event.entityLiving.posZ + (double)(1.0F/References.rand.nextInt(10));
		
			world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, xPos, yPos, zPos, 0.0D, 0.0D, 0.0D);
		}
	}
}

