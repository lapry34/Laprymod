package me.lapry.events;

import me.lapry.utils.LapryLog;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;

public class PlayerLoginServerEvent {

	@SubscribeEvent
	public void onPlayerJoinDedicated(PlayerLoggedInEvent event){
		if(event.player.getEntityWorld().isRemote){
		
			String s = MinecraftServer.getServer().getMinecraftVersion();
			
			event.player.addChatComponentMessage(new ChatComponentTranslation(String.format(s)));
			LapryLog.getLogger().info(s);
		}
	}
}
