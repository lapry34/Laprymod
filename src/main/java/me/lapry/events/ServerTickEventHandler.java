package me.lapry.events;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ServerTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ServerTickEventHandler {

	@SideOnly(Side.SERVER)
	@SubscribeEvent
	public void onServerTick(ServerTickEvent event){
		
		if(event.phase == Phase.START){}  
		else if(event.phase == Phase.END){}
	}
	
}
