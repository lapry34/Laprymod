package me.lapry.events;

import org.lwjgl.input.Keyboard;

import me.lapry.utils.LapryLog;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;;

public class KeyBindings {

	public KeyBinding Enter;
	
	public KeyBindings(){
		
		this.Enter = new KeyBinding("key.enterLapry", Keyboard.KEY_RETURN, "key.categories.Laprymod");
		
		
		ClientRegistry.registerKeyBinding(Enter);
	}
	
	@SubscribeEvent
	public void onKeyPressed(KeyInputEvent event){
		
		if(this.Enter.isPressed())
			LapryLog.getLogger().info("Key Enter Has Been Pressed.");
	}
}
