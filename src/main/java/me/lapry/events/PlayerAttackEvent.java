package me.lapry.events;

import me.lapry.utils.References;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class PlayerAttackEvent {

	@SubscribeEvent
	public void onPlayerAttacked(AttackEntityEvent event){
		
		World world = event.target.worldObj;
		
		for(int i = 0; i < 13; i++){
			
			double xPos = event.target.posX + (double)(1.0F/References.rand.nextInt(10));
			double yPos = event.target.posY + References.rand.nextInt(2);
			double zPos = event.target.posZ + (double)(1.0F/References.rand.nextInt(10));
			
			world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, xPos, yPos, zPos, 0.0D, 0.0D, 0.0D);
		}
	}
}