package me.lapry;

import me.lapry.blocks.LapryBlock;
import me.lapry.blocks.LapryOre;
import me.lapry.blocks.te.LapryBlockTE;
import me.lapry.commands.ExtinguishCommand;
import me.lapry.commands.GmAdventureCommand;
import me.lapry.commands.GmCommand;
import me.lapry.commands.GmCreativeCommand;
import me.lapry.commands.GmSpectatorCommand;
import me.lapry.commands.GmSurvivalCommand;
import me.lapry.commands.HealCommand;
import me.lapry.commands.SpeedCommand;
import me.lapry.commands.SuicideCommand;
import me.lapry.commands.TpsCommand;
import me.lapry.commands.VanishCommand;
import me.lapry.events.GuiEvent;
import me.lapry.events.MobAttackEvent;
import me.lapry.events.PlayerAttackEvent;
import me.lapry.items.LapryIngot;
import me.lapry.items.armors.LapryBody;
import me.lapry.items.armors.LapryBoots;
import me.lapry.items.armors.LapryHelm;
import me.lapry.items.armors.LapryLegs;
import me.lapry.items.foods.GrayApple;
import me.lapry.items.tools.LapryAxe;
import me.lapry.items.tools.LapryPick;
import me.lapry.items.tools.LapryShowel;
import me.lapry.items.tools.LaprySword;
import me.lapry.proxy.CommonProxy;
import me.lapry.utils.References;
import me.lapry.world.gen.CustomWorldGenerator;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.IWorldGenerator;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.Mod.InstanceFactory;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = References.MODID, name = References.MOD_NAME, version = References.VERSION, acceptedMinecraftVersions = References.MC_VERSION)

public final class Main extends LapryModContainer {
	
	@Instance
	public static Main instance = new Main();
	
    @SidedProxy(clientSide = References.CLIENT_PROXY_CLASS, serverSide = References.SERVER_PROXY_CLASS)
	public static CommonProxy proxy;
	
    @InstanceFactory
    public static Main getInstance(){ 
    	return instance;
    }
    
    public static CreativeTabs tabLapry;
    public static IWorldGenerator customWorldGenerator;
    public static ToolMaterial enumToolLapry;
    public static ArmorMaterial enumArmorLapry;
    public static Block lapryBlock;
    public static Block lapryOre; 
    public static Item lapryIngot; //http://codeshare.io/n5fAE http://codeshare.io/5HAhM
    public static Item grayApple; 
    public static Item laprySword;
    public static Item lapryPick;
    public static Item lapryAxe;
    public static Item lapryShowel;
    public static Item lapryHelm;
    public static Item lapryBody;
    public static Item lapryLegs;
    public static Item lapryBoots; 
 
	@EventHandler
    public void preload(FMLPreInitializationEvent event){
		
		tabLapry = new TabLapry(CreativeTabs.getNextID(), "Laprymod");
		enumToolLapry = EnumHelper.addToolMaterial("Lapry", 5, 394, 10.0F, 8, 24);
		enumArmorLapry = EnumHelper.addArmorMaterial("lapry", "lapry", 40, new int[]{6,11,13,7}, 15);
    	lapryBlock = new LapryBlock();
    	lapryOre = new LapryOre();
    	lapryIngot = new LapryIngot();
    	grayApple = new GrayApple();
    	laprySword = new LaprySword();
    	lapryPick = new LapryPick();
    	lapryAxe = new LapryAxe();
    	lapryShowel = new LapryShowel();
    	lapryHelm = new LapryHelm();
    	lapryBody = new LapryBody();
    	lapryLegs = new LapryLegs();
    	lapryBoots = new LapryBoots();	
    	customWorldGenerator = new CustomWorldGenerator();
    	GameRegistry.addRecipe(new ItemStack(lapryBlock), new Object[]{"ooo","ooo","ooo",'o', lapryIngot});
    	GameRegistry.addRecipe(new ItemStack(lapryHelm), new Object[]{"lll","l l","   ",'l', lapryIngot});
    	GameRegistry.addRecipe(new ItemStack(lapryLegs), new Object[]{"lll","l l","l l",'l', lapryIngot});
    	GameRegistry.addRecipe(new ItemStack(lapryBoots), new Object[]{"   ","l l","l l",'l', lapryIngot});
    	GameRegistry.addRecipe(new ItemStack(laprySword), new Object[]{" l "," l "," i ",'l', lapryIngot,'i', Items.stick});
    	GameRegistry.addRecipe(new ItemStack(lapryPick), new Object[]{"lll"," i "," i ",'l', lapryIngot,'i', Items.stick});
    	GameRegistry.addRecipe(new ItemStack(lapryAxe), new Object[]{"ll ","li "," i ",'l', lapryIngot,'i', Items.stick});
    	GameRegistry.addRecipe(new ItemStack(lapryShowel), new Object[]{" l "," i "," i ",'l', lapryIngot,'i', Items.stick});
    	GameRegistry.addSmelting(new ItemStack(lapryOre), new ItemStack(lapryIngot), 1.0F);
    	GameRegistry.addSmelting(new ItemStack(Items.apple), new ItemStack(grayApple), 1.0F); 
    	
    } 
    																	
    @EventHandler
    public void load(FMLInitializationEvent event){
    	
    	proxy.registerRenderThings();
    	proxy.registerSided();
    	
    	GameRegistry.registerTileEntity(LapryBlockTE.class, "LapryBlockTE");
     	MinecraftForge.EVENT_BUS.register(new GuiEvent());
    	MinecraftForge.EVENT_BUS.register(new MobAttackEvent());
    	MinecraftForge.EVENT_BUS.register(new PlayerAttackEvent());
    	
    }

    @EventHandler
    public void onServerStarting(FMLServerStartingEvent event){
    	
        event.registerServerCommand(new GmCommand());
    	event.registerServerCommand(new GmSpectatorCommand());
    	event.registerServerCommand(new GmCreativeCommand());
    	event.registerServerCommand(new GmSurvivalCommand());
    	event.registerServerCommand(new GmAdventureCommand());
    	event.registerServerCommand(new VanishCommand());
 	    event.registerServerCommand(new SpeedCommand());
    	event.registerServerCommand(new ExtinguishCommand());    	
    	event.registerServerCommand(new HealCommand());
    	event.registerServerCommand(new SuicideCommand()); 
    	event.registerServerCommand(new TpsCommand());
    }

}
/*
    Laprymod Copyright � 2015 Lapry
  
    This program and the accompanying materials
    are made available the terms of the GNU Lesser General Public License v2.1
    as published by the Free Software Foundation, either version 2.1
    or (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>.
  
    You can found all sources at <https://gitlab.com/lapry34/Laprymod/tree/master>.

*/
