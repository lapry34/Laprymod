package me.lapry.utils;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.IChatComponent;
import net.minecraft.world.World;

public class CommandHelper {

	public EntityPlayer player;
	public ICommandSender sender;
	public EntityPlayerMP mp;
	public World world;
	
	public CommandHelper(ICommandSender sender){
		
		EntityPlayer player = (EntityPlayer) sender;
		
		World world = sender.getEntityWorld();
		
		EntityPlayerMP mp = (EntityPlayerMP) sender; //(EntityPlayerMP) sender.getEntityWorld().getPlayerEntityByName(sender.getName());
		
		this.sender = sender;
		this.player = player;
		this.mp = mp;
		this.world = world;
	}
	
	public EntityPlayer getPlayer(){
		return this.player;
	}
	
	public ICommandSender getSender(){
		return this.sender;
	}
	
	public EntityPlayerMP getMP(){
		return this.mp;
	}
	
	public World getWorld(){
		return this.world;
	}
	
	public void sendMessage(String message){
		
		IChatComponent chat =  new ChatComponentTranslation(String.format(message).replaceAll("&", "�"));

		this.getPlayer().addChatComponentMessage(chat);

	}
}
