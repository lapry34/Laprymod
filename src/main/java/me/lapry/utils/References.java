package me.lapry.utils;

import java.util.Random;

public class References {

	public static final String MODID = "lapry";
    public static final String VERSION = "1.1-SNAPSHOT-b51";
    public static final String MOD_NAME = "laprymod";
    public static final String MC_VERSION = "1.8";
    public static final String CLIENT_PROXY_CLASS = "me.lapry.proxy.ClientProxy";
    public static final String SERVER_PROXY_CLASS = "me.lapry.proxy.ServerProxy";
	public static final Random rand = new Random();
}
