package me.lapry.utils;

import org.apache.logging.log4j.Level;
import net.minecraftforge.fml.common.FMLLog;

public class LapryLog {
    
	public static final LapryLog logger = new LapryLog("Lapry");

    public static LapryLog getLogger(){
        return logger;
    }
    
    private final String tag;

    protected LapryLog(String tag){
        this.tag = tag;
    }

    private void log(Level level, String message){
        FMLLog.log(this.tag, level, String.format(message));
    }
    
    private void log(Level level, Throwable t, String message){
        FMLLog.log(this.tag, level, t, String.format(message));
    }

    public void warning(String message){
        this.log(Level.WARN, message);
    }

    public void info(String message){
    	this.log(Level.INFO, message);
    }

    public void error(String message, Throwable t){
        this.log(Level.ERROR, t, message);
    }
    
    public void debug(String message){
        this.log(Level.DEBUG, message);
    }
    
    public void finer(String message){
    	this.log(Level.TRACE, message);
    }
    
    public void fatal(String message, Throwable t){
        this.log(Level.FATAL, t, message);
    }
    public void fatal(String message){
        this.log(Level.FATAL, message);
    }
    
    public void error(String message){
        this.log(Level.ERROR, message);
    }

}