package me.lapry.utils.maths;

public class LibGDXMath {
    
	public static final float PI = JavaMath.PI;
    public static final float E = JavaMath.E;
    
    private static final int SIN_BITS = 14; // 16KB. Adjust for accuracy.
    private static final int SIN_MASK = ~(-1 << SIN_BITS);
    private static final int SIN_COUNT = SIN_MASK + 1;

    public static final float TWO_PI = JavaMath.TWO_PI;
    public static final float HALF_PI = JavaMath.HALF_PI;
    private static final float degFull = 360;
    private static final float radToIndex = SIN_COUNT / TWO_PI;
    private static final float degToIndex = SIN_COUNT / degFull;

    public static final float degreesToRadians = PI / 180;

    private static final float[] table = new float[SIN_COUNT];

    static {
        for(int i = 0; i < SIN_COUNT; i++) {
            table[i] = (float)Math.sin((i + 0.5F) / SIN_COUNT * TWO_PI);
        }
        for (int i = 0; i < 360; i += 90) {
            table[(int)(i * degToIndex) & SIN_MASK] = (float)Math.sin(i * degreesToRadians);
        }
    }


    public static float sin(float radians) {
        return table[(int)(radians * radToIndex) & SIN_MASK];
    }

    public static float cos(float radians) {
        return table[(int)((radians + HALF_PI) * radToIndex) & SIN_MASK];
    }
    
    public static float abs(float p){
        return p >= 0.0F ? p : -p;
    }
    
}
