package me.lapry.utils.maths;

public class RivensFullMath {

    private static final float SIN_TO_COS;
    private static final int SIN_BITS, SIN_MASK, SIN_COUNT;
    private static final float radFull, radToIndex;
    private static final float[] sinFull;

    static {
        SIN_TO_COS = (float) (Math.PI * 0.5f);

        SIN_BITS = 12;
        SIN_MASK = ~(-1 << SIN_BITS);
        SIN_COUNT = SIN_MASK + 1;

        radFull = JavaMath.TWO_PI;
        radToIndex = SIN_COUNT / radFull;

        sinFull = new float[SIN_COUNT];
        for (int i = 0; i < SIN_COUNT; i++) {
            sinFull[i] = (float) Math.sin((i + Math.min(1, i % (SIN_COUNT / 4)) * 0.5) / SIN_COUNT * radFull);
        }
    }

    public static float sin(float rad) {
        return sinFull[(int)(rad * radToIndex) & SIN_MASK];
    }

    public static float cos(float rad) {
        return sin(rad + SIN_TO_COS);
    }

    public static float abs(float p){
        return p >= 0.0F ? p : -p;
    }
    
}
