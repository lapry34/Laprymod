package me.lapry.utils.maths;

public class RivensMath {

    private static final int SIN_BITS, SIN_MASK, SIN_COUNT;
    private static final float radFull, radToIndex;
    private static final float degFull, degToIndex;
    private static final float[] sin, cos;

    static {
        SIN_BITS  = 12;
        SIN_MASK  = ~(-1 << SIN_BITS);
        SIN_COUNT = SIN_MASK + 1;

        radFull = JavaMath.TWO_PI ;
        degFull = 360.0F;
        radToIndex = SIN_COUNT / radFull;
        degToIndex = SIN_COUNT / degFull;

        sin = new float[SIN_COUNT];
        cos = new float[SIN_COUNT];

        for (int i = 0; i < SIN_COUNT; i++) {
            sin[i] = (float) JavaMath.sin((i + 0.5f) / SIN_COUNT * radFull);
            cos[i] = (float) JavaMath.cos((i + 0.5f) / SIN_COUNT * radFull);
        }

        // Four cardinal directions (credits: Nate)
        for (int i = 0; i < 360; i += 90) {
            sin[(int)(i * degToIndex) & SIN_MASK] = JavaMath.sin((float)(i * Math.PI / 180.0));
            cos[(int)(i * degToIndex) & SIN_MASK] = JavaMath.cos((float)(i * Math.PI / 180.0));
        }
    }

    public static float sin(float rad) {
        return sin[(int)(rad * radToIndex) & SIN_MASK];
    }

    public static float cos(float rad) {
        return cos[(int)(rad * radToIndex) & SIN_MASK];
    }
    
    public static float abs(float p){
        return p >= 0.0F ? p : -p;
    }

}
