package me.lapry.utils.maths;

public class RivensHalfMath {

    private static final float SIN_TO_COS;
    private static final int SIN_BITS, SIN_MASK, SIN_MASK2, SIN_COUNT, SIN_COUNT2;
    private static final float radFull, radToIndex;
    private static final float[] sinHalf;

    static {
        SIN_TO_COS = (float)JavaMath.HALF_PI;

        SIN_BITS = 12;
        SIN_MASK = ~(-1 << SIN_BITS);
        SIN_MASK2 = SIN_MASK >> 1;
        SIN_COUNT = SIN_MASK + 1;
        SIN_COUNT2 = SIN_MASK2 + 1;

        radFull = JavaMath.TWO_PI;
        radToIndex = SIN_COUNT / radFull;

        sinHalf = new float[SIN_COUNT2];
        for (int i = 0; i < SIN_COUNT2; i++) {
            sinHalf[i] = (float) Math.sin((i + Math.min(1, i % (SIN_COUNT / 4)) * 0.5) / SIN_COUNT * radFull);
        }
    }

    public static float sin(float rad) {
        int index1 = (int) (rad * radToIndex) & SIN_MASK;
        int index2 = index1 & SIN_MASK2;
        //TODO: int mul = (((index1 - index2) >>> 31) << 1) + 1;
        int mul = ((index1 == index2) ? +1 : -1);
        return sinHalf[index2] * mul;
    }

    public static float cos(float rad) {
        return sin(rad + SIN_TO_COS);
    }
    
    public static float abs(float p){
        return p >= 0.0F ? p : -p;
    }
    
}
