package me.lapry.utils.maths;

public class JavaMath {

	public static final float PI = (float)Math.PI;
	public static final float E = (float)Math.E;
	public static final float TWO_PI = PI * 2;
	public static final float HALF_PI = PI / 2;
	
    public static float sin(float radians) {
        return (float)Math.sin(radians);
    }

    public static float cos(float radians) {
        return (float)Math.cos(radians);
    }
    
    public static float abs(float a){
    	return (float)Math.abs(a);
    }
}
