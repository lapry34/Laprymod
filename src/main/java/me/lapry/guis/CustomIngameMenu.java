package me.lapry.guis;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.gui.GuiYesNoCallback;

@SuppressWarnings("unused")
public class CustomIngameMenu extends GuiIngameMenu {
	
	@Override
	public void initGui(){
		
		super.initGui();
		for(Object obj : this.buttonList){
			
			if(obj instanceof GuiButton){
				
				GuiButton button = (GuiButton) obj;
				
				if(button.id == 12){
					button.enabled = false;
					button.visible = false;
				}
			}
		}
	}

}
