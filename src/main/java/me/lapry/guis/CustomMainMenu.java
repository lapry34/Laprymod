package me.lapry.guis;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMainMenu;

public class CustomMainMenu extends GuiMainMenu {

    @Override
	public void initGui(){
		
    	super.initGui();
    	
    	this.splashText = "Lapry!";
		
		for(Object obj : this.buttonList)
			if(obj instanceof GuiButton){
				GuiButton button = (GuiButton)obj;
				
				if(button.id == 6){
					button.enabled = false;
					button.visible = false;
				}else if (button.id == 14){
					button.enabled = false;
					button.visible = false;
				}
			}
	}
	
	@Override
	public void updateScreen(){};
	
	/* TODO Make The Default Background
	@Override
	public void rotateAndBlurSkybox(float f1){};
	
	@Override
	public void renderSkybox(int i1, int i2, float f1){};
	
	@Override
	public void drawPanorama(int i1, int i2, float f1){};
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks){
   
		this.drawDefaultBackground();
        Tessellator tessellator = Tessellator.getInstance();
        WorldRenderer worldrenderer = tessellator.getWorldRenderer();
        short short1 = 274;
        int k = this.width / 2 - short1 / 2;
        byte b0 = 30;
        this.drawGradientRect(0, 0, this.width, this.height, -2130706433, 16777215);
        this.drawGradientRect(0, 0, this.width, this.height, 0, Integer.MIN_VALUE);
        this.mc.getTextureManager().bindTexture(minecraftTitleTextures);
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);

        if ((double)this.updateCounter < 1.0E-4D)
        {
            this.drawTexturedModalRect(k + 0, b0 + 0, 0, 0, 99, 44);
            this.drawTexturedModalRect(k + 99, b0 + 0, 129, 0, 27, 44);
            this.drawTexturedModalRect(k + 99 + 26, b0 + 0, 126, 0, 3, 44);
            this.drawTexturedModalRect(k + 99 + 26 + 3, b0 + 0, 99, 0, 26, 44);
            this.drawTexturedModalRect(k + 155, b0 + 0, 0, 45, 155, 44);
        }
        else
        {
            this.drawTexturedModalRect(k + 0, b0 + 0, 0, 0, 155, 44);
            this.drawTexturedModalRect(k + 155, b0 + 0, 0, 45, 155, 44);
        } 

        worldrenderer.setColorOpaque_I(4210752); //-1 16777215
        GlStateManager.pushMatrix();
        GlStateManager.translate((float)(this.width / 2 + 90), 70.0F, 0.0F);
        GlStateManager.rotate(-20.0F, 0.0F, 0.0F, 1.0F); 
        float f1 = 1.8F - LibGDXMath.abs(LibGDXMath.sin((float)(Minecraft.getSystemTime() % 1000L) / 1000.0F * JavaMath.TWO_PI) * 0.1F);
        f1 = f1 * 100.0F / (float)(this.fontRendererObj.getStringWidth(this.splashText) + 32); 
        GlStateManager.scale(f1, f1, f1); 
        this.drawCenteredString(this.fontRendererObj, this.splashText, 0, -8, -256);
        //GlStateManager.popMatrix();
        String s = "Minecraft 1.8";

        if (this.mc.isDemo())
        {
            s = s + " Demo";
        }

        java.util.List<String> brandings = com.google.common.collect.Lists.reverse(net.minecraftforge.fml.common.FMLCommonHandler.instance().getBrandings(true));
        for (int i = 0; i < brandings.size(); i++)
        {
            String brd = brandings.get(i);
            if (!com.google.common.base.Strings.isNullOrEmpty(brd))
            {
                this.drawString(this.fontRendererObj, brd, 2, this.height - ( 10 + i * (this.fontRendererObj.FONT_HEIGHT + 1)), 16777215);
            }
        }
        net.minecraftforge.client.ForgeHooksClient.renderMainMenu(this, this.fontRendererObj, this.width, this.height);
        String s1 = "Copyright Mojang AB. Do not distribute!";
        this.drawString(this.fontRendererObj, s1, this.width - this.fontRendererObj.getStringWidth(s1) - 2, this.height - 10, -1);

        if (this.openGLWarning1 != null && this.openGLWarning1.length() > 0)
        {
            drawRect(this.field_92022_t - 2, this.field_92021_u - 2, this.field_92020_v + 2, this.field_92019_w - 1, 1428160512);
            this.drawString(this.fontRendererObj, this.openGLWarning1, this.field_92022_t, this.field_92021_u, -1);
            this.drawString(this.fontRendererObj, this.openGLWarning2, (this.width - this.field_92024_r) / 2, ((GuiButton)this.buttonList.get(0)).yPosition - 12, -1);
        } 

       super.drawScreen(mouseX, mouseY, partialTicks);
        
	}
	
	*/
	
	
}


