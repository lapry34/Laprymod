package me.lapry;

import java.awt.Desktop;
import java.io.File;
import java.util.Map;
import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.minecraftforge.fml.common.versioning.DefaultArtifactVersion;
import net.minecraftforge.fml.common.versioning.VersionParser;
import net.minecraftforge.fml.relauncher.FMLInjectionData;
import net.minecraftforge.fml.relauncher.IFMLCallHook;
import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin;
import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin.MCVersion;;

@MCVersion("1.8")
public class CoreMain implements IFMLLoadingPlugin, IFMLCallHook {

	public static final Logger logger = LogManager.getLogger("Lapry Core");
	public static File minecraftDir;
	
	public static final String mcVersion = "[1.8]";
    public static final String version = "${mod_version}";
    public static String currentMcVersion;
	
    public CoreMain(){
    	if(minecraftDir != null)
    		return;
    	
    	
    	minecraftDir = (File)FMLInjectionData.data()[6];
    	currentMcVersion = (String)FMLInjectionData.data()[4];
    	
    }
    
    
	@Override
    public String[] getASMTransformerClass(){
		versionCheck(mcVersion, "LapryCore");
        return new String[]{"me.lapry.transformers.LapryASMTransformer"};
    }

    @Override
    public String getModContainerClass(){
        return "me.lapry.Main";
    }

    @Override
    public String getSetupClass() {
        return this.getClass().getName();
    }

    @Override
    public void injectData(Map<String, Object> data){};

    @Override
	public String getAccessTransformerClass() {
    	return "me.lapry.transformers.LapryAccessTransformer";
	}

	@Override
	public Void call() throws Exception {
		return null;
	}
	
	public static void versionCheck(String reqVersion, String mod){
        String mcVersion = (String) FMLInjectionData.data()[4];
        if (!VersionParser.parseRange(reqVersion).containsVersion(new DefaultArtifactVersion(mcVersion))){
        	String err = "This version of " + mod + " does not support minecraft version " + mcVersion;
            logger.error(err);
            JEditorPane ep = new JEditorPane("text/html","<html>" + err +"<br>Remove it from your coremods folder" + "</html>");
            ep.setEditable(false);
            ep.setOpaque(false);
            ep.addHyperlinkListener(new HyperlinkListener(){
            	@Override
                public void hyperlinkUpdate(HyperlinkEvent event){
                    try {
                        if (event.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED))
                            Desktop.getDesktop().browse(event.getURL().toURI());
                    } catch (Exception e){logger.warn("Unhandled Exception during VersionCheck");}
                }
            });

            JOptionPane.showMessageDialog(null, ep, "Fatal error", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
    }

}