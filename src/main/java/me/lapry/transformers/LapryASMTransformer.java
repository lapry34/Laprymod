package me.lapry.transformers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import net.minecraft.launchwrapper.IClassTransformer;
import net.minecraft.launchwrapper.LaunchClassLoader;

@SuppressWarnings("unused")
public class LapryASMTransformer implements IClassTransformer {

	private static final Logger logger = LogManager.getLogger("Lapry Core");
	
	private static ArrayList<IClassTransformer> delegatedTransformers;
	  private static Method m_defineClass;
	  private static Field f_cachedClasses;
	
    public LapryASMTransformer(){
    	
        delegatedTransformers = new ArrayList<IClassTransformer>();
        try {
            m_defineClass = ClassLoader.class.getDeclaredMethod("defineClass", String.class, byte[].class, Integer.TYPE, Integer.TYPE);
            m_defineClass.setAccessible(true);
            f_cachedClasses = LaunchClassLoader.class.getDeclaredField("cachedClasses");
            f_cachedClasses.setAccessible(true);
        }
        catch(Exception e){
        	throw new RuntimeException(e);
        }
    } 
	
	@Override
	public byte[] transform(String name, String transformedName, byte[] bytes){
	
		return bytes;
	}

	
	public static byte[] readFully(InputStream stream) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream(stream.available());
	    int r;
	    while ((r = stream.read()) != -1)
	    	bos.write(r);
	       
	    return bos.toByteArray();
	}

}