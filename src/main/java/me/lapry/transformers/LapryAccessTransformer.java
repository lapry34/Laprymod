package me.lapry.transformers;

import java.io.IOException;

import net.minecraftforge.fml.common.asm.transformers.AccessTransformer;

public class LapryAccessTransformer extends AccessTransformer {
	
	public LapryAccessTransformer() throws IOException {
        super("lapry_at.cfg");
    }
}
