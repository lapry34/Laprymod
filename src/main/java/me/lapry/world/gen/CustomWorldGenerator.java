package me.lapry.world.gen;

import java.util.Random;

import me.lapry.Main;
import me.lapry.world.gen.feature.CustomWorldGenMinable;
import net.minecraft.block.state.pattern.BlockHelper;
import net.minecraft.init.Blocks;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraftforge.fml.common.IWorldGenerator;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class CustomWorldGenerator implements IWorldGenerator {

	public CustomWorldGenerator(){
		GameRegistry.registerWorldGenerator(this, 1);
	}
	
	@Override
	public void generate(Random rand, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator,IChunkProvider chunkProvider) {
		switch(world.provider.getDimensionId()){
		
		case 0: 
			generateSurface(world, rand, chunkX, chunkZ);
			break;
			
		case -1:
			generateNether(world, rand, chunkX, chunkZ);
			break;
			
		case 1:
			generateEnd(world, rand, chunkX, chunkZ);
			break;
		
		}
	}
	private void generateSurface(World world, Random rand, int chunkX, int chunkZ){
			
		for (int i = 0; i < 9; i++){// 9 = rarita'
			
			int x = chunkX + rand.nextInt(16);
			int y = 10 + rand.nextInt(30); //massima altezza
			int z = chunkZ + rand.nextInt(16);
			// 12 = numero di blocchi per "nodo"
           
			CustomWorldGenMinable wgm = new CustomWorldGenMinable(Main.lapryOre.getDefaultState(), 12, BlockHelper.forBlock(Blocks.stone));
            
			wgm.generate(world, rand, new BlockPos(x,y,z));
		}
		
	}
	
	private void generateNether(World world, Random rand, int chunkX, int chunkZ){
		
	}

	private void generateEnd(World world, Random rand, int chunkX, int chunkZ){
		
	}

}

