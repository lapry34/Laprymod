package me.lapry.world.gen.feature;

import java.util.Random;
import net.minecraft.block.BlockFlower;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;

@SuppressWarnings("unused")
public class CustomWorldGenFlowers extends WorldGenerator {
    private BlockFlower flower;
    private IBlockState blockState;
	private static final String __OBFID = "CL_00000410";

    public CustomWorldGenFlowers(BlockFlower flower, BlockFlower.EnumFlowerType flowerType){
    	
        this.setGeneratedBlock(flower, flowerType);
    }

    public void setGeneratedBlock(BlockFlower flower, BlockFlower.EnumFlowerType flowerType)
    {
        this.flower = flower;
        this.blockState = flower.getDefaultState().withProperty(flower.getTypeProperty(), flowerType);
    }

    public boolean generate(World world, Random rand, BlockPos pos)
    {
        for (int i = 0; i < 64; ++i){
        	
            BlockPos blockpos = pos.add(rand.nextInt(8) - rand.nextInt(8), rand.nextInt(4) - rand.nextInt(4), rand.nextInt(8) - rand.nextInt(8));

            if (world.isAirBlock(blockpos) && (!world.provider.getHasNoSky() || blockpos.getY() < 255) && this.flower.canBlockStay(world, blockpos, this.blockState)){
            	
                world.setBlockState(blockpos, this.blockState, 2);
            }
        }

        return true;
    }
}