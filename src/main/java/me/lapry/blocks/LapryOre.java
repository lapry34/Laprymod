package me.lapry.blocks;

import me.lapry.Main;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemStack;
import net.minecraft.util.WeightedRandomChestContent;
import net.minecraftforge.common.ChestGenHooks;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;

public class LapryOre extends Block {

	private static final String name = "lapryOre";
	
	public LapryOre() {
		super(Material.rock);
		setUnlocalizedName(name);
		setCreativeTab(Main.tabLapry);
		setHardness(4.0F);
		setResistance(20.0F);
		setStepSound(soundTypeStone);
		setHarvestLevel("pickaxe", 2);
		GameRegistry.registerBlock(this, name);
		OreDictionary.registerOre(name, this);
    	ChestGenHooks.getInfo(ChestGenHooks.DUNGEON_CHEST).addItem(new WeightedRandomChestContent(new ItemStack(this),3,5,25));
    	ChestGenHooks.getInfo(ChestGenHooks.PYRAMID_DESERT_CHEST).addItem(new WeightedRandomChestContent(new ItemStack(this),3,5,10));
    	ChestGenHooks.getInfo(ChestGenHooks.PYRAMID_JUNGLE_CHEST).addItem(new WeightedRandomChestContent(new ItemStack(this),3,5,10));
    	ChestGenHooks.getInfo(ChestGenHooks.VILLAGE_BLACKSMITH).addItem(new WeightedRandomChestContent(new ItemStack(this),3,5,10));
    	ChestGenHooks.getInfo(ChestGenHooks.MINESHAFT_CORRIDOR).addItem(new WeightedRandomChestContent(new ItemStack(this),3,5,10));
    	ChestGenHooks.getInfo(ChestGenHooks.NETHER_FORTRESS).addItem(new WeightedRandomChestContent(new ItemStack(this),3,5,10));
    	ChestGenHooks.getInfo(ChestGenHooks.STRONGHOLD_CORRIDOR).addItem(new WeightedRandomChestContent(new ItemStack(this),3,5,10));
    	ChestGenHooks.getInfo(ChestGenHooks.STRONGHOLD_CROSSING).addItem(new WeightedRandomChestContent(new ItemStack(this),3,5,10));
    	
	}
	
	public String getName(){
		return name;
	}
	
}
