package me.lapry.blocks.te;

import java.util.ArrayList;

import com.google.common.collect.Lists;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.gui.IUpdatePlayerListBox;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;

public class LapryBlockTE extends TileEntity implements IUpdatePlayerListBox {

	public ArrayList<String> visitors = Lists.newArrayList();
	
	@Override
	public void updateContainingBlockInfo(){
		super.updateContainingBlockInfo();
	}
	
	@Override
	public BlockPos getPos(){
		return super.getPos();
	}
	@Override
	public void readFromNBT(NBTTagCompound nbt){
		super.readFromNBT(nbt);
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt){
		super.writeToNBT(nbt);
	}

	@Override
	public void update(){}
	
}

