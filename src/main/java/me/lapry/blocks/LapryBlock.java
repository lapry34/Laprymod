package me.lapry.blocks;

import me.lapry.Main;
import me.lapry.blocks.te.LapryBlockTE;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;

public class LapryBlock extends Block implements ITileEntityProvider {

	private static final String name = "lapryBlock";
	private static final String te = name + "TE";
	
	public LapryBlock() {
		super(Material.iron);
		setUnlocalizedName(name);
		setCreativeTab(Main.tabLapry);
		setHardness(4.0F);
		setResistance(20.0F);
		setHarvestLevel("pickaxe", 2);
		setStepSound(soundTypeMetal);
		GameRegistry.registerBlock(this, name);
    	OreDictionary.registerOre(name, this);
		}
	
	@Override
	public TileEntity createNewTileEntity(World world, int meta) {
		
		return new LapryBlockTE();
	}

	@Override
	public void breakBlock(World world, BlockPos pos, IBlockState state){
		
		super.breakBlock(world, pos, state);
	}
	
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack par1){
		return EnumRarity.EPIC;
	}
	
	public String getName(){
		return name;
	}
	
	public String getNameTE(){
		return te;
	}
	
}
