package me.lapry.items;

import me.lapry.Main;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;

public class LapryIngot extends Item {

	private static final String name = "lapryIngot";
	
	public LapryIngot(){
		setUnlocalizedName(name);
		setCreativeTab(Main.tabLapry);
		GameRegistry.registerItem(this, name);
    	OreDictionary.registerOre(name, this);
		
	}
	
	@Override
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player){
	        
	    player.addPotionEffect(new PotionEffect(Potion.absorption.getId(), 600, 4));
	  
	    return stack;
	 }
	
	public String getName(){
		return name;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack par1){
		return EnumRarity.EPIC;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean hasEffect(ItemStack par1){
		return true;
	}
	
}
