package me.lapry.items.armors;

import me.lapry.Main;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class LapryHelm extends ItemArmor {

	private static final String name = "lapryHelm";
	
	public LapryHelm(){
		super(Main.enumArmorLapry, 1, 0);
		setCreativeTab(Main.tabLapry);
		setUnlocalizedName(name);
    	GameRegistry.registerItem(this, name);
	}
	
	public String getName(){
		return name;
	}
	
	@Override
	public void onArmorTick(World world, EntityPlayer player, ItemStack itemStack) {
	    if (itemStack.getItem() == Main.lapryHelm) {
	    	if (player.getActivePotionEffect(Potion.resistance) == null || player.getActivePotionEffect(Potion.resistance).getDuration() <= 3)
	            player.addPotionEffect(new PotionEffect(Potion.resistance.getId(), 10, 5, true, true));
	    	
	    }
	} 
	
	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack par1){
		return EnumRarity.EPIC;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean hasEffect(ItemStack par1){
		return true;
	}
	
    public String getLanguageName(){
    	return name + ".name";
    }
	
}
