package me.lapry.items.foods;

import me.lapry.Main;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;

public class GrayApple extends ItemFood {
	
	private static final String name = "grayApple";
	
	public GrayApple(){
		super(6, 4.0F, false);
		setCreativeTab(Main.tabLapry);
		setUnlocalizedName(name);
    	GameRegistry.registerItem(this, name);
    	OreDictionary.registerOre(name, this);
	}
	
	public String getName(){
		return name;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack par1){
		return EnumRarity.RARE;
	}
	
    public String getLanguageName(){
    	return name + ".name";
    }
	
}
