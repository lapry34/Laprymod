package me.lapry.items.tools;

import me.lapry.Main;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class LapryAxe extends ItemAxe {

	private static final String name = "lapryAxe";
	
	public LapryAxe(){
		super(Main.enumToolLapry);
		setCreativeTab(Main.tabLapry);
		setUnlocalizedName(name);
    	GameRegistry.registerItem(this, name);
	}
	
	public String getName(){
		return name;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack par1){
		return EnumRarity.EPIC;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean hasEffect(ItemStack par1){
		return true;
	}
	
    public String getLanguageName(){
    	return name + ".name";
    }
	
}
