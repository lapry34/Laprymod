package me.lapry;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.launchwrapper.ITweaker;
import net.minecraft.launchwrapper.Launch;
import net.minecraft.launchwrapper.LaunchClassLoader;

@SuppressWarnings({"rawtypes","unchecked"})
public class LapryTweaker implements ITweaker {

	public static File minecraftDir;
	
	private List<String> args;
    private static final String[] TRANSFORMERS = new String[]{"me.lapry.transformers.LapryASMTransformer"};
 
    private final String[] EXCLUDED = new String[]{}; 
	
	@Override
	public void acceptOptions(List<String> args, File gameDir, File assetsDir, String profile){
		 this.args = new ArrayList<String>(args);
	        this.args.add("--version");
	        this.args.add(profile);
	        if(assetsDir != null) {
	            this.args.add("--assetsDir");
	            this.args.add(assetsDir.getAbsolutePath());
	        }
	        if(gameDir != null) {
	            this.args.add("--gameDir");
	            this.args.add(gameDir.getAbsolutePath());
	        }
	        if(minecraftDir == null)
	        	minecraftDir = gameDir;
	}

	@Override
	public void injectIntoClassLoader(LaunchClassLoader classLoader){
		for(String transformer : TRANSFORMERS) 
            classLoader.registerTransformer(transformer);

        for(String excluded : EXCLUDED) 
            classLoader.addTransformerExclusion(excluded);
	}

	@Override
	public String getLaunchTarget(){
		return "net.minecraft.client.main.Main";
	}

	@Override
	public String[] getLaunchArguments(){
		ArrayList args = (ArrayList)Launch.blackboard.get("ArgumentList");
        if(args.isEmpty()) args.addAll(this.args);

        this.args = null;

        return new String[0];
	}

}
