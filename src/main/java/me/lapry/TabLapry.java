package me.lapry;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TabLapry extends CreativeTabs {
	
	public TabLapry(int pos, String name) {
	    super(pos, name);
	}

	@SideOnly(Side.CLIENT)
	public Item getTabIconItem() {
		return Main.laprySword;
	}
	
	@SideOnly(Side.CLIENT)
	public String getTranslatedTabLabel(){
	   return "Lapry";
	}
}
